const jwtMiddleware = require(__base + 'src/app/middlewares/jwt-middleware')
const jwt = require('jsonwebtoken')
const config = require(__base + 'config.testjwt')

const jwtController = {    
    protected: (req, res) => {
        jwtMiddleware(req, res, () => {
            res.status(200).json({message: 'I\'m a protected route'})
        })
    },
    token: (req, res) => {
        const user = {
            id: 25,
            username: 'guest'
        }    
        var token = jwt.sign(user, config.JWT_SECRET, {
            expiresIn: '2h' // expires in 2 hours
        })    
        user.token = token    
        res.json(user)
    },
    free: (req, res) => {
        res.status(200).json({message: "I'm a free route"})
    }
}

module.exports = jwtController
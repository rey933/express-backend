const routes = require('express').Router()
const controllers = require(__base + 'src/app/controllers/jwt-controller')

routes.get('/protected', controllers.protected)
routes.get('/token', controllers.token)
routes.get('/free', controllers.free)

module.exports = routes

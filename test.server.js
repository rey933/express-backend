global.__base = __dirname + '/' // Setting root path in a global scope

const express = require('express')
const app = express()
const routes = require('./src/app/routes/api')


app.use('/', routes)

module.exports = app
const testServer = require('../../test.server')
const request = require('supertest')

test('When a Client Make a Request to a Protected Route and an Invalid JWT is given, return 403', async () => {
    let response = await request(testServer)
    						.get('/protected')
                            .set('Accept', 'application/json')
                            .set('Authorization', 'WrongToken')
    expect(response.statusCode).toBe(403)
})

test('When a Client Make a Request to a non Protected Route, return 200', async () => {
    let response = await request(testServer).get('/free')
    expect(response.statusCode).toBe(200)
})

test('When a Client Make a Request to a Protected route and VALID JWT is given, return 200', async (done) => {

    let response = request(testServer)

    response.get('/token') //Getting the token
        .type('json')
        .end((err, res) => {
            if(err) return done(err)
            console.log(res.body)
            response.get('/protected')
                .set('Authorization', res.body.token)          
                .end((err, res) => {
                    expect(res.statusCode).toBe(200)
                    done()
                })
        })
})

